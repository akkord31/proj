from rest_framework import serializers

from cars.models import Country, Spares, Car


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class SparesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Spares
        fields = '__all__'


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = '__all__'
