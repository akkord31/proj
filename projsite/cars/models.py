from django.db import models


class Country(models.Model):
    name = models.CharField(verbose_name='Страна производитель', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Производитель'
        verbose_name_plural = 'Производители'


class Spares(models.Model):
    name = models.CharField(verbose_name='Марка автомобиля', max_length=100, null=True)
    spare = models.CharField(verbose_name='Запчасть', max_length=100, null=True)
    country = models.ForeignKey(Country, verbose_name='Страна производитель', on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Запчасти'
        verbose_name_plural = 'Запчасти'


class Car(models.Model):
    mark = models.CharField(verbose_name='Марка автомобиля', max_length=50)
    model = models.CharField(verbose_name='Модель автомобиля', max_length=80)
    price = models.IntegerField(verbose_name='Цена автомобиля')
    year = models.DateField(verbose_name='Год производства автомобиля')
    country = models.ForeignKey(Country, verbose_name='Страна производитель', on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.mark

    class Meta:
        verbose_name = 'Автомобиль'
        verbose_name_plural = 'Автомобили'
