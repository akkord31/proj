from django.shortcuts import render
from rest_framework import viewsets

from cars.models import Country, Spares, Car
from cars.serializers import CountrySerializer, SparesSerializer, CarSerializer


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class SparesViewSet(viewsets.ModelViewSet):
    queryset = Spares.objects.all()
    serializer_class = SparesSerializer


class CarViewSet(viewsets.ModelViewSet):
    queryset = Car.objects.all()
    serializer_class = CarSerializer